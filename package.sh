#! /usr/bin/env bash
cd src
pipenv lock --requirements > requirements.txt
pip3 install --target ./package -r requirements.txt
cd package
zip -r ../package.zip .
cd ..
zip -g package.zip handler.py
rm -rf package