resource "aws_s3_bucket" "bucket_a" {
  bucket_prefix = "source-bucket-a-"
  acl           = "private"
  force_destroy = true # For demo purposes only
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.bucket_a.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.exif_stripper.arn
    events              = ["s3:ObjectCreated:*"]
    filter_suffix       = ".jpg"
  }
}

resource "aws_s3_bucket" "bucket_b" {
  bucket_prefix = "destination-bucket-b-"
  acl           = "private"
  force_destroy = true # For demo purposes only
}
