locals {
  deployment_package_path = "${path.module}/src/package.zip"
}
# Lambda permissions to read from bucket A and write to bucket B

resource "aws_lambda_function" "exif_stripper" {
  function_name    = "exif-remover"
  runtime          = "python3.8"
  handler          = "handler.lambda_handler"
  role             = aws_iam_role.lambda_execution.arn
  filename         = local.deployment_package_path
  source_code_hash = filebase64sha256(local.deployment_package_path)
  environment {
    variables = {
      "dest_bucket_arn" = aws_s3_bucket.bucket_b.arn
    }
  }
}

resource "aws_lambda_permission" "allow_s3_events" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.exif_stripper.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.bucket_a.arn
}

resource "aws_iam_role" "lambda_execution" {
  assume_role_policy  = data.aws_iam_policy_document.execution_trust.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]
  inline_policy {
    name   = "lambda-exif-stripper-read-write-s3"
    policy = data.aws_iam_policy_document.execution_policy.json
  }
}

data "aws_iam_policy_document" "execution_trust" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "execution_policy" {
  statement {
    effect = "Allow"
    actions = [
      "s3:GetObject"
    ]
    resources = ["${aws_s3_bucket.bucket_a.arn}/*"]
  }
  statement {
    effect = "Allow"
    actions = [
      "s3:PutObject"
    ]
    resources = ["${aws_s3_bucket.bucket_b.arn}/*"]
  }
  statement {
    effect    = "Allow"
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.bucket_a.arn, aws_s3_bucket.bucket_b.arn]
  }
}

# data "archive_file" "package" {
#   type        = "zip"
#   source_dir  = "src/package"
#   output_path = "./package.zip"
#   source {
#     content  = file("src/handler.py")
#     filename = "handler.py"
#   }
# }

