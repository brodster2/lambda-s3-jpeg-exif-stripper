install-deps:
	cd src && pipenv sync

test:
	cd src && pipenv run python -m unittest -v

package:
	bash package.sh

init:
	terraform init

deploy: package init
	terraform apply
