# lambda-s3-jpeg-exif-stripper

An event driven AWS Lambda function written in Python 3 that will remove exif metadata from JPEG files when uploaded to S3.

## Required software

- Terraform version 0.15+
- python 3.8
- Pipenv
- GNU Make

### To deploy

1. Have your cli authenticated to your aws account (exported env vars or via awsume).
1. Run `make install-deps` to install the Python dependencies with Pipenv
1. From the root directory run `make deploy`. This will package the Python code with dependencies into a zipfile which Terraform will then deploy as part of the `aws_lambda_function` resource.

### Current issues

- I've had trouble packaging the python dependencies for the lambda seeing as my environment is Mac and the Lambda runs on linux. The PIL library for image manipulation is compiled to the specific operating system.

### Future improvements

- Monitoring the memory usage of the lambda memory usage, as it holds all image data in memory, large images could cause problems.
- Some sort of retry mechanism. If the lambda starts failing, images wont be sent to the destination bucket and left in the source bucket. Could possibly use EventBridge replays or some sort of sqs dead-letter queue to save upload events to retry.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14 |
| <a name="requirement_archive"></a> [archive](#requirement\_archive) | 2.2.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 3.49.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.49.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_role.lambda_execution](https://registry.terraform.io/providers/hashicorp/aws/3.49.0/docs/resources/iam_role) | resource |
| [aws_lambda_function.exif_stripper](https://registry.terraform.io/providers/hashicorp/aws/3.49.0/docs/resources/lambda_function) | resource |
| [aws_lambda_permission.allow_s3_events](https://registry.terraform.io/providers/hashicorp/aws/3.49.0/docs/resources/lambda_permission) | resource |
| [aws_s3_bucket.bucket_a](https://registry.terraform.io/providers/hashicorp/aws/3.49.0/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.bucket_b](https://registry.terraform.io/providers/hashicorp/aws/3.49.0/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket_notification.bucket_notification](https://registry.terraform.io/providers/hashicorp/aws/3.49.0/docs/resources/s3_bucket_notification) | resource |
| [aws_iam_policy_document.execution_policy](https://registry.terraform.io/providers/hashicorp/aws/3.49.0/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.execution_trust](https://registry.terraform.io/providers/hashicorp/aws/3.49.0/docs/data-sources/iam_policy_document) | data source |

## Inputs

No inputs.

## Outputs

No outputs.
