import json
import urllib.parse
import boto3
import logging
import io
import hashlib
from os import environ
from PIL import Image, ExifTags

s3 = boto3.client('s3')

def remove_exif_data(image_bytes):
    if isinstance(image_bytes, bytes):

        try:
            image = Image.open(io.BytesIO(image_bytes))

            data = list(image.getdata())
            image_without_exif = Image.new(image.mode, image.size)
            image_without_exif.putdata(data)
            s = io.BytesIO()
            image_without_exif.save(s, format="jpeg")
            return s
        except Exception as e:
            logging.error("Error while removing exif data")
            logging.error(e)
            raise e

    else:
        raise TypeError("Image input is not a bytes type object")

def lambda_handler(event, context):
    #print("Received event: " + json.dumps(event, indent=2))

    # Get the object from the event and show its content type
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    try:
        get_response = s3.get_object(Bucket=bucket, Key=key)
    except Exception as e:
        logging.error(e)
        logging.error('Error getting object {} from bucket {}. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucket))
        raise e

    try:
        image_without_exif = remove_exif_data(get_response['Body'])
        image_without_exif.seek(0)
    except Exception as e:
        logging.error(e)
        logging.error(f"Error stripping exif data from image {key}")
        raise e
    
    # Write image to s3
    dest_bucket = environ['dest_bucket_arn']
    try:
        put_response = s3.put_object(
            Bucket=dest_bucket,
            Key=key,
            Body=image_without_exif.read(),
            ContentMD5=hashlib.md5(image_without_exif.read())
        )
    except Exception as e:
        logging.error(e)
        logging.error(f"Failed to upload file {key}")
        raise e
    