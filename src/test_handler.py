import unittest
import io
from PIL import Image
from handler import remove_exif_data

class TestRemoveExif(unittest.TestCase):

    def test_happy_path(self):
        with io.open("test_image.jpg", "rb", buffering = 0) as file:
            stripped_bytes = remove_exif_data(file.read())
            stripped_bytes.seek(0)
            self.assertGreater(len(stripped_bytes.read()), 0) # Make sure there is actually image data
            with Image.open(stripped_bytes) as i:
                img_exif = i.getexif()
                self.assertEqual(img_exif, {}) # Check that exif tags are empty

    def test_values(self):
        self.assertRaises(TypeError, remove_exif_data, 20)
        self.assertRaises(TypeError, remove_exif_data, "hello")
